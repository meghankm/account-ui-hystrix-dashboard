package com.meghan.accountui.controller;

import com.meghan.accountui.model.Account;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

@RestController
public class AccountUIController {

    //interceptors will do client side load balancing
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder){
        return restTemplateBuilder.build();
    }

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getAccountFromSecondarySource",
            commandKey = "getAccountKey",
            threadPoolKey = "getAccountThread",
            commandProperties = {
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "2"),
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000")
            },
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "4"),
                    @HystrixProperty(name = "maxQueueSize", value = "10")
            })
    @RequestMapping(value = "/accounts/{accountId}", method = RequestMethod.GET)
    public @ResponseBody
    Account getAccount(@PathVariable String accountId) {
        System.out.println(new Date()+" : "+Thread.currentThread().getName()+" : Fetching Account details from primary downstream system");
        return  restTemplate.getForObject("http://config-account-service/accounts/"+accountId, Account.class);
    }

    @HystrixCommand(fallbackMethod = "getAccountFailFast",
            commandKey = "getSecAccountKey",
            threadPoolKey = "getSecAccountThread",
            commandProperties = {
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "2"),
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000")
            },
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "4"),
                    @HystrixProperty(name = "maxQueueSize", value = "10")
            })
    public @ResponseBody
    Account getAccountFromSecondarySource(@PathVariable String accountId, Throwable throwable) throws Exception {
        System.out.println(new Date()+" : "+Thread.currentThread().getName()+" : Primary account service is down. Fetching Account details from secondary source");
        try {
            return getDataFromSecondaryStore();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public @ResponseBody
    Account getAccountFailFast(@PathVariable String accountId, Throwable throwable) {
        System.out.println(new Date()+" : "+Thread.currentThread().getName()+" : Secondary source is down too. Fetching default Account details from fail fast method");
        return new Account(0, "default account", new BigDecimal(0.0));
    }

    private Account getDataFromSecondaryStore() throws Exception {
        //return new Account(1, "saving", new BigDecimal(1000.00));
        throw new Exception();
    }
}
